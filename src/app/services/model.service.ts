import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Model } from '../models/model.model';

@Injectable({
  providedIn: 'root'
})
export class ModelService {

  constructor() { }

  private models: Model[] = [
    {
      nazwa: "model1",
      data_na: new Date(2020, 12, 8),
      preprocesor: "preprocessor1",
      postprocesor: "postprocessor1",
      zbior_danych: "zbior danych",
      frakcja_testowa: 0.3,
      n_trees: 5,
      interaction_depth: 10,
      shrinkage: 0.035,
      n_minobsinnode: 15,
      cv_folds: 2,
      threshold: 0.5,
    },
    {
      nazwa: "model2",
      data_na: new Date(2020, 11, 10),
      preprocesor: "preprocessor2",
      postprocesor: "postprocessor2",
      zbior_danych: "zbior danych",
      frakcja_testowa: 0.3,
      n_trees: 5,
      interaction_depth: 10,
      shrinkage: 0.035,
      n_minobsinnode: 15,
      cv_folds: 2,
      threshold: 0.5,
    },
    {
      nazwa: "model3",
      data_na: new Date(2020, 10, 15),
      preprocesor: "preprocessor3",
      postprocesor: "postprocessor3",
      zbior_danych: "zbior danych",
      frakcja_testowa: 0.3,
      n_trees: 5,
      interaction_depth: 10,
      shrinkage: 0.035,
      n_minobsinnode: 15,
      cv_folds: 2,
      threshold: 0.5,
    },
    {
      nazwa: "model4",
      data_na: new Date(2020, 7, 7),
      preprocesor: "preprocessor4",
      postprocesor: "postprocessor4",
      zbior_danych: "zbior danych",
      frakcja_testowa: 0.3,
      n_trees: 5,
      interaction_depth: 10,
      shrinkage: 0.035,
      n_minobsinnode: 15,
      cv_folds: 2,
      threshold: 0.5,
    },
  ];

  private models$ = new BehaviorSubject<Model[]>(this.models);

  private count: number = 1;

  getModelsStream() {
    return this.models$.asObservable();
  }

  addModel(){
    this.models.push({
      nazwa: `Nowy Model${this.count++}`,
      data_na: new Date(),
      preprocesor: "preprocessor",
      postprocesor: "postprocessor",
      zbior_danych: "zbior danych",
      frakcja_testowa: 0.3,
      n_trees: 5,
      interaction_depth: 10,
      shrinkage: 0.035,
      n_minobsinnode: 15,
      cv_folds: 2,
      threshold: 0.5,
    });
    this.models$.next(this.models);
  }

  removeModel(nazwa: string){
    this.models = this.models.filter(x => x.nazwa !== nazwa);
    this.models$.next(this.models);
  }

  calculateFraud(model: Model){
    return Math.floor(Math.random() * 100) + 1;
  }

}
