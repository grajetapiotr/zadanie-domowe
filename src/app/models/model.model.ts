export interface Model {
    nazwa: string,
    data_na: Date,
    frakcja_testowa: 0.3,
    n_trees: 5,
    interaction_depth: 10,
    shrinkage: 0.035,
    n_minobsinnode: 15,
    cv_folds: 2,
    threshold: 0.5,
    preprocesor: string,
    postprocesor: string 
    zbior_danych: string,
}