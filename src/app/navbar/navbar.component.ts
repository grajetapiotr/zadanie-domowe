import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../login/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private auth: AuthService, private router: Router) {
  }

  logged$!: Observable<Boolean>;
  logout() {
    this.auth.logoutUser();
    this.router.navigate(['/login']);
  }
  ngOnInit(): void {
    this.logged$ = this.auth.getLoggedStream();
  }

}
