import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router) { }
  ngOnInit(): void {
  }

  hide = true;
  error = false;
  loginForm = this.fb.group({
    login: ['', Validators.required],
    password: ['', Validators.required],
  });

  get login() { return this.loginForm.get('login'); }
  get password() { return this.loginForm.get('password'); }

  onSubmit() {
    if(this.auth.loginUser(this.loginForm.get('login')?.value, this.loginForm.get('password')?.value)){
      this.router.navigate(['']);
    }else{
      this.error = true;
      this.loginForm.reset();
    }
  }

}
