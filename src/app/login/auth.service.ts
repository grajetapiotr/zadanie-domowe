import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LoginComponent } from './login.component';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  isLogged$ = new BehaviorSubject<Boolean>(false);

  loginUser(login: string, password: string) {
    if (login === 'admin' && password === 'admin') {
      localStorage.setItem('token', 'fake-token');
      this.isLogged$.next(true);
      return true;
    }else{
      return false;
    }
  }

  getLoggedStream(){
    return this.isLogged$.asObservable();
  }

  logoutUser(){
    localStorage.removeItem('token');
    this.isLogged$.next(false);
  }

  loggedIn() {
    return !!localStorage.getItem('token');
  }
}
