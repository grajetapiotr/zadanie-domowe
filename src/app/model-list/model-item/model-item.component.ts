import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Model } from 'src/app/models/model.model';
import { ModelService } from 'src/app/services/model.service';
import { ModelDeleteComponent } from './model-delete/model-delete.component';
import { ModelDetailsComponent } from './model-details/model-details.component';

@Component({
  selector: 'app-model-item',
  templateUrl: './model-item.component.html',
  styleUrls: ['./model-item.component.scss']
})
export class ModelItemComponent implements OnInit {

  constructor(public dialog: MatDialog, private model_service: ModelService) { }
  openDialog(){
    this.dialog.open(ModelDetailsComponent, {data: this.model, width: '800px'});
  }
  @Input() model!: Model;
  fraud: number = 0;
  ngOnInit(): void {
  }

  calculateFraud(){
    this.fraud = this.model_service.calculateFraud(this.model);
  }

  delete(){
    let dialogRef = this.dialog.open(ModelDeleteComponent);
    dialogRef.afterClosed().subscribe(
      result =>{
        if(result === true){
          this.model_service.removeModel(this.model.nazwa);
        }
      }
    )
  }

}