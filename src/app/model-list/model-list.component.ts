import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Model } from '../models/model.model';
import { ModelService } from '../services/model.service';
import { MatDialog } from '@angular/material/dialog';
import { AddModelComponent } from './add-model/add-model.component';

@Component({
  selector: 'app-model-list',
  templateUrl: './model-list.component.html',
  styleUrls: ['./model-list.component.scss']
})
export class ModelListComponent implements OnInit {

  constructor(public dialog: MatDialog, private model_service: ModelService) { }
  openDialog(){
    this.dialog.open(AddModelComponent);
  }
  models$!: Observable<Model[]>;

  ngOnInit(): void {
    this.models$ = this.model_service.getModelsStream();
  }

  addNewModel(){
    this.openDialog();
  }

}
