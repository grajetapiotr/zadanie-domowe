import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ModelService } from 'src/app/services/model.service';

@Component({
  selector: 'app-add-model',
  templateUrl: './add-model.component.html',
  styleUrls: ['./add-model.component.scss']
})
export class AddModelComponent implements OnInit {

  constructor(private model_service: ModelService) { }

  ngOnInit(): void {
  }

  @ViewChild('fileInput') fileInput!: ElementRef;
  fileToUpload: File | null = null;
  fileAttr = 'Choose File';
  fileUploaded = false;

  uploadFileEvt(imgFile: any) {
    if (imgFile.target.files && imgFile.target.files[0]) {
      this.fileAttr = imgFile.target.files[0].name;
      this.fileUploaded = true;
      this.fileToUpload = imgFile.target.files[0];
    } else {
      this.fileAttr = 'Choose File';
      this.fileUploaded = false;
    }
  }

  addModel(){
    // here we would send the file to the backend API
    this.model_service.addModel();
  }


}
