# ZadanieDomowe
Stworzona została warstwa frontend, autentykacja oraz inne funkcjonalności korzystające z backend zostały zmockowane po stronie frontendu.

## Development server

```bash
git clone https://gitlab.com/grajetapiotr/zadanie-domowe.git
cd zadanie-domowe
npm install
cd src
ng serve
```

## Live demo

 Zadanie zostało wrzucone na netifly [Zadanie Domowe](https://affectionate-hawking-7bf979.netlify.app) login: admin password: admin
